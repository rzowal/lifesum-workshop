//
//  LifesumServices.swift
//  LifesumWorkshopTests
//
//  Created by Rafal Zowal on 2018-01-24.
//  Copyright © 2018 ZovalHB. All rights reserved.
//

import XCTest
@testable import LifesumWorkshop

class LifesumServicesTest: XCTestCase {
        
        var sut: LifesumService?
        
        override func setUp() {
            super.setUp()
            sut = LifesumService()
        }
        
        override func tearDown() {
            sut = nil
            super.tearDown()
        }
        
        func test_fetch_popular_photos() {
            
            // Given A apiservice
            let sut = self.sut!
            
            // When fetch popular photo
            let expect = XCTestExpectation(description: "callback")
            
            sut.fetchPhotos(complete: { (success, photos, error) in
                expect.fulfill()
                XCTAssertEqual( photos.count, 20)
                for photo in photos {
                    XCTAssertNotNil(photo.id)
                }
            })
            
            wait(for: [expect], timeout: 0.5)
        }
        
}
