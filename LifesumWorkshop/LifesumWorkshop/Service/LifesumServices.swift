//
// Created by Rafal Zowal on 2018-01-24.
// Copyright (c) 2018 ZovalHB. All rights reserved.
//

import Foundation


class LifesumService {
    func fetchPhotos(complete: @escaping(_ success: Bool, _ photos:[Photo], _ error: Error?)->() ) {
        DispatchQueue.global().async {
            let path = Bundle.main.path(forResource: "content", ofType: "json")
            if let path = path {
                let data = try! Data(contentsOf: URL(fileURLWithPath: path))
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                let photos = try! decoder.decode(Photos.self, from: data)
                complete(true, photos.photos, nil)
            }
        }
    }
}