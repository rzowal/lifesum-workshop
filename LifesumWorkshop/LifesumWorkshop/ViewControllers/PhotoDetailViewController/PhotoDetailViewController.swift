//
// Created by Rafal Zowal on 2018-01-24.
// Copyright (c) 2018 ZovalHB. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoDetailViewController: UIViewController  {

    var imageUrl: String?

    @IBOutlet private weak var imageView: UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let imageUrl = imageUrl, let imageView = imageView {
            imageView.sd_setImage(with: URL(string: imageUrl)) { (image, error, type, url) in

            }
        }
    }
}
